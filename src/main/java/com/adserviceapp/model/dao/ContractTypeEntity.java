package com.adserviceapp.model.dao;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "contract_type")
public class ContractTypeEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "contract_type_id")
    private Long contractTypeId;

    @Column(name = "contract_type")
    @NotEmpty
    @NotNull
    @Size(min = 3, max = 45)
    private String contractType;

    @ManyToMany
    private List<AdvertisementEntity> advertisements;

}
