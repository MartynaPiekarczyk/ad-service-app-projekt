package com.adserviceapp.model.dao;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id")
    private Long userId;

    @Column(name = "username", unique = true)
    @NotEmpty
    @NotNull
    @Size(min = 6, max = 30, message = "Username must be at least 6 and at most 30 characters long.")
    private String username;

    @Column(name = "password")
    @NotEmpty
    @NotNull
    @Size(min = 8, max = 1000)
    private String password;

    @Column(name = "email", unique = true)
    @NotEmpty
    @NotNull
    @Email
    private String email;

    @OneToMany
    private List<AdvertisementEntity> advertisements;

    @ManyToOne
    private RoleEntity role;

    @ManyToMany
    private Set<AdvertisementEntity> favorite;
}
