package com.adserviceapp.model.dao;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "advertisement")
public class AdvertisementEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "advertisement_Id")
    private Long advertisementId;

    @Column(name= "title")
    @NotEmpty
    @NotNull
    @Size(min = 5, max = 120)
    private String title;

    @Column(name = "content")
    @NotEmpty
    @NotNull
    @Size(min = 20, max = 10_000)
    private String content;

    @Column(name = "company_name")
    @NotEmpty
    @NotNull
    @Size(min = 4, max = 100)
    private String companyName;

    @Column(name = "publishing_date")
    @NotNull
    private LocalDateTime publishingDate;

    @Column(name = "salary")
    @NotNull
    private Long salary;

    @OneToOne
    private UserEntity user;

    @ManyToOne
    private LocationEntity location;

    @ManyToOne
    private ExperienceEntity experience;

    @ManyToMany
    private List<ContractTypeEntity> contractType;

    @ManyToMany
    private List<TechnologyEntity> technology;

}
