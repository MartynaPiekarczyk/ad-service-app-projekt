package com.adserviceapp.model.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Builder
@Getter
@Setter
public class AdvertisementResponseDto {

    private Long advertisementId;
    private String title;
    private LocalDateTime publishingDate;

}
