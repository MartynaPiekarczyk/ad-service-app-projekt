package com.adserviceapp.model.dto.converters;

import com.adserviceapp.model.dao.AdvertisementEntity;
import com.adserviceapp.model.dto.AdvertisementResponseDto;
import org.springframework.stereotype.Component;

@Component
public class AdvertisementResponseConverter implements Converter<AdvertisementEntity, AdvertisementResponseDto> {

    @Override
    public AdvertisementResponseDto convert(AdvertisementEntity adverisementEntity) {
        return AdvertisementResponseDto.builder()
                .advertisementId(adverisementEntity.getAdvertisementId())
                .publishingDate(adverisementEntity.getPublishingDate())
                .title(adverisementEntity.getTitle())
                .build();
    }

    @Override
    public <T, R> boolean canHandle(Class<T> from, Class<R> to) {
        return from.equals(AdvertisementEntity.class) && to.equals(AdvertisementResponseDto.class);
    }
}
