package com.adserviceapp.model.dto.converters;

import com.adserviceapp.model.dao.UserEntity;
import com.adserviceapp.model.dto.UserResponseDto;
import org.springframework.stereotype.Component;

@Component
public class UserResponseConverter implements Converter<UserEntity, UserResponseDto> {

    @Override
    public UserResponseDto convert(UserEntity entity) {
        return UserResponseDto.builder()
                .username(entity.getUsername())
                .email(entity.getEmail())
                .build();
    }

    @Override
    public <T, R> boolean canHandle(Class<T> from, Class<R> to) {
        return from.equals(UserEntity.class) && to.equals(UserResponseDto.class);
    }
}
