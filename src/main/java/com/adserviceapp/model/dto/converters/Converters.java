package com.adserviceapp.model.dto.converters;

import com.adserviceapp.exception.ApplicationError;
import com.adserviceapp.exception.ApplicationException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class Converters {

    private final List<Converter> converters;

    public <T, R> R convert(T fromObject, Class<R> to) {
        for (Converter conv : converters) {
            if (conv.canHandle(fromObject.getClass(), to)) {
                return to.cast(conv.convert(fromObject));
            }
        }
        throw new ApplicationException(ApplicationError.CONVERTER_NOT_FOUND);
    }
}
