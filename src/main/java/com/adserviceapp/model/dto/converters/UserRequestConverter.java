package com.adserviceapp.model.dto.converters;

import com.adserviceapp.model.dao.UserEntity;
import com.adserviceapp.model.dto.UserRequestDto;
import com.adserviceapp.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserRequestConverter implements Converter<UserRequestDto, UserEntity> {

    private final PasswordEncoder encoder;
    private final RoleRepository repository;

    @Override
    public UserEntity convert(UserRequestDto request) {
        UserEntity entity = new UserEntity();
        entity.setUsername(request.getUsername());
        entity.setPassword(encoder.encode(request.getPassword()));
        entity.setEmail(request.getEmail());
        entity.setRole(repository.getById(request.getRoleId()));
        return entity;
    }

    @Override
    public <T, R> boolean canHandle(Class<T> from, Class<R> to) {
        return from.equals(UserRequestDto.class) && to.equals(UserEntity.class);
    }
}
