package com.adserviceapp.model.dto.converters;

import com.adserviceapp.model.dao.AdvertisementEntity;
import com.adserviceapp.model.dto.AdvertisementRequestDto;
import com.adserviceapp.repository.ContractTypeRepository;
import com.adserviceapp.repository.ExperienceRepository;
import com.adserviceapp.repository.LocationRepository;
import com.adserviceapp.repository.TechnologyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
@RequiredArgsConstructor
public class AdvertisementRequestConverter implements Converter<AdvertisementRequestDto, AdvertisementEntity> {

    private final LocationRepository locationRepository;
    private final TechnologyRepository technologyRepository;
    private final ExperienceRepository experienceRepository;
    private final ContractTypeRepository contractTypeRepository;

    @Override
    public AdvertisementEntity convert(AdvertisementRequestDto request) {
        AdvertisementEntity entity = new AdvertisementEntity();
        entity.setContent(request.getContent());
        entity.setCompanyName(request.getCompanyName());
        entity.setContractType(contractTypeRepository.findAllById(request.getContractTypeIds()));
        entity.setExperience(experienceRepository.getById(request.getExperienceId()));
        entity.setLocation(locationRepository.getById(request.getLocationId()));
        entity.setSalary(request.getSalary());
        entity.setTechnology(technologyRepository.findAllById(request.getTechnologyIds()));
        entity.setTitle(request.getTitle());
        entity.setPublishingDate(LocalDateTime.now());
        return entity;
    }

    @Override
    public <T, R> boolean canHandle(Class<T> from, Class<R> to) {
        return from.equals(AdvertisementRequestDto.class) && to.equals(AdvertisementEntity.class);
    }
}

