package com.adserviceapp.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class UserRequestDto {

    private Long roleId;

    @NotEmpty
    @Length(min = 6, max = 30, message = "Nazwa użytkownika musi mieć minimalnie 6, a maksymalnie 30 znaków.")
    private String username;

    @NotEmpty
    @Length(min = 8, max = 30, message = "Hasło musi mieć minimalnie 8, a maksymalnie 30 znaków.")
    private String password;

    @NotEmpty
    @Email(message = "Nieprawidłowy adres mailowy. Sprawdź swój adres.")
    private String email;

}