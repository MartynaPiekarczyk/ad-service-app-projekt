package com.adserviceapp.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AdvertisementRequestDto {

    @NotEmpty
    @Size(min = 10, max = 120)
    private String title;

    @NotEmpty
    @Size(min = 20, max = 10_000)
    private String content;

    @NotEmpty
    @Size(min = 4, max = 100)
    private String companyName;

    private Long salary;

    private Long locationId;

    private Long experienceId;

    private List<Long> contractTypeIds;

    private List<Long> technologyIds;


}
