package com.adserviceapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdServiceAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdServiceAppApplication.class, args);
    }

}
