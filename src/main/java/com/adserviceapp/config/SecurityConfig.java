package com.adserviceapp.config;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

@EnableWebSecurity
@Configuration
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserDetailsService userDetailsService;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/", "/home", "/user/register","/user/registration-success", "/advertisement/**", "/delete-fav/**").permitAll()
                .antMatchers("/css/**", "/img/**").permitAll()
                .antMatchers("/add-fav/**").hasRole("USER")
                .anyRequest().hasAnyRole("EMPLOYER", "ADMIN").and()
//                .anyRequest().permitAll().and()
                .formLogin().permitAll()
                .loginPage("/user/login")
//                .loginProcessingUrl("/process/login")
                .defaultSuccessUrl("/home", true)
//                .permitAll()
//                .failureUrl("/user/login")
//                .and()
                .and()
                .logout()
                .logoutUrl("/logout")
                .logoutSuccessUrl("/home")
                .deleteCookies("JSESSIONID")
                .and().csrf().disable();


    }
}
