package com.adserviceapp.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.time.LocalDateTime;

@RestControllerAdvice
@Slf4j
public class ExceptionHandlers {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ApplicationErrorResponse> handleGlobal(Exception e) {
        log.error(e.getMessage());
        e.printStackTrace();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                .body(ApplicationErrorResponse.builder()
                        .errorMessage("Unknown Error Occurred")
                        .internalErrorCode("UE-666")
                        .exceptionTime(LocalDateTime.now())
                        .build()
                );
    }

    @ExceptionHandler(ApplicationException.class)
    public ResponseEntity<ApplicationErrorResponse> handleApplicationException(ApplicationException exception) {
        log.error(exception.getMessage());
        exception.printStackTrace();
        return ResponseEntity.status(HttpStatus.valueOf(exception.getErrorStatus()))
                .body(ApplicationErrorResponse.builder()
                        .errorMessage(exception.getMessage())
                        .internalErrorCode(exception.getInternalErrorCode())
                        .exceptionTime(exception.getExceptionTime())
                        .build()
                );
    }
}
