package com.adserviceapp.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ApplicationError {

    ADVERTISEMENT_DOESNT_EXIST(
            "Advertisement with id (%s) not exists",
            400,
            "ADV_NOT_FOUND_ERR-400"
    ),
    CONVERTER_NOT_FOUND(
            "Contact with admin or pull the trigger",
            500,
            "CONV_NOT_FOUND_ERR-500"
    ),
    GENERAL_ERROR(
            "Something wrong happened",
            500,
            "I_DONT_KNOW_WHAT_HAPPENED"
    ),
    LOCATION_NOT_FOUND(
            "Cannot find such location",
            500,
            "LOC_NOT_FOUND_ERR-500"
    ),
    EXPERIENCE_NOT_FOUND(
            "Cannot find such experience level",
            500,
            "XP_NOT_FOUND_ERR-500"
    ),
    TECHNOLOGY_NOT_FOUND(
            "Cannot find such technology",
            500,
            "TECH_NOT_FOUND_ERR-500"
    ),
    CONTRACT_TYPE_NOT_FOUND(
            "Cannot find such contract type",
            500,
            "CONTR_NOT_FOUND_ERR-500"
    ),
   ROLE_NOT_FOUND(
            "Cannot find such technology",
            500,
            "TECH_NOT_FOUND_ERR-500"
    ),
    USER_DOES_NOT_EXIST(
            "Cannot find such user",
            500,
            "USER_NOT_FOUND_ERR-500"
    );

    private final String messageTemplate;
    private final int httpStatus;
    private final String internalErrorCode;
}
