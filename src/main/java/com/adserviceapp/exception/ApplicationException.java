package com.adserviceapp.exception;

import lombok.Getter;

import java.time.LocalDateTime;

@Getter
public class ApplicationException extends RuntimeException{

    private final LocalDateTime exceptionTime;
    private final int errorStatus;
    private final String internalErrorCode;

    public ApplicationException(ApplicationError error, Object ... messageParams) {
        super(String.format(error.getMessageTemplate(), messageParams));
        this.exceptionTime = LocalDateTime.now();
        this.errorStatus = error.getHttpStatus();
        this.internalErrorCode = error.getInternalErrorCode();
    }
}
