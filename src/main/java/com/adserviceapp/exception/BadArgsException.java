package com.adserviceapp.exception;

import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@Getter
@Setter
public class BadArgsException extends IllegalArgumentException{

    private Map<String,String> errors = new HashMap<>();

}
