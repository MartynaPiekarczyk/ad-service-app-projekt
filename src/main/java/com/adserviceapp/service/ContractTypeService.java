package com.adserviceapp.service;

import com.adserviceapp.exception.ApplicationError;
import com.adserviceapp.exception.ApplicationException;
import com.adserviceapp.model.dao.ContractTypeEntity;
import com.adserviceapp.repository.ContractTypeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ContractTypeService {

    private final ContractTypeRepository repository;

    public List<ContractTypeEntity> listAllContractTypes() {
        return repository.findAll();
    }

    public List<ContractTypeEntity> listContractTypesById(Long id) {
        return repository.findAllById(Collections.singleton(id));
    }


    public ContractTypeEntity getContractTypeById(Long id) {
        return repository.findById(id).orElseThrow(() -> new ApplicationException(ApplicationError.CONTRACT_TYPE_NOT_FOUND, id));
    }
}
