package com.adserviceapp.service;

import com.adserviceapp.exception.ApplicationError;
import com.adserviceapp.exception.ApplicationException;
import com.adserviceapp.model.dao.TechnologyEntity;
import com.adserviceapp.repository.TechnologyRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TechnologyService {

    private final TechnologyRepository repository;

    public List<TechnologyEntity> listAllTechnologies() {
        return repository.findAll();
    }

    public TechnologyEntity getTechnologyById(Long id) {
        return repository.findById(id).orElseThrow(() -> new ApplicationException(ApplicationError.TECHNOLOGY_NOT_FOUND, id));
    }

    public List<TechnologyEntity> listTechnologyById(Long id) {
        return repository.findAllById(Collections.singleton(id));
    }
}
