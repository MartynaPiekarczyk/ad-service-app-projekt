package com.adserviceapp.service;

import com.adserviceapp.exception.ApplicationError;
import com.adserviceapp.exception.ApplicationException;
import com.adserviceapp.model.dao.AdvertisementEntity;
import com.adserviceapp.model.dao.UserEntity;
import com.adserviceapp.model.dto.AdvertisementRequestDto;
import com.adserviceapp.model.dto.AdvertisementResponseDto;
import com.adserviceapp.model.dto.converters.Converters;
import com.adserviceapp.repository.AdvertisementRepository;
import com.adserviceapp.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@RequiredArgsConstructor
public class AdvertisementService {

    private final AdvertisementRepository advertisementRepository;
    private final Converters converters;
    private final UserRepository userRepository;


    @Transactional
    public AdvertisementResponseDto addAdvertisement(AdvertisementRequestDto request, String userName) {
        AdvertisementEntity newEntity = converters.convert(request, AdvertisementEntity.class);
        UserEntity user = userRepository.findByUsername(userName).orElseThrow();
        newEntity.setUser(user);
        AdvertisementEntity savedObject = advertisementRepository.save(newEntity);
        List<AdvertisementEntity> advertisements = user.getAdvertisements();
        if(advertisements==null){
            advertisements=new ArrayList<>();
            user.setAdvertisements(advertisements);
        }
        advertisements.add(newEntity);
        return converters.convert(savedObject, AdvertisementResponseDto.class);
    }

    public List<AdvertisementEntity> displayAllAdvertisements() {
        return advertisementRepository.findAll();
    }

    public List<AdvertisementEntity> displayAddByUsername(String username){
        return advertisementRepository.findAllByUserUsername(username);
    }

    public Set<AdvertisementEntity> displayFavByUsername(String username){
        return userRepository.findByUsername(username).orElseThrow().getFavorite();
    }

    @Transactional
    public void deleteFavByUsername(String username, Long favAdvertisementId){
        AdvertisementEntity favAdvertisement = advertisementRepository.findById(favAdvertisementId).orElseThrow();
        userRepository.findByUsername(username).orElseThrow(() -> new ApplicationException(ApplicationError.GENERAL_ERROR))
                .getFavorite().remove(favAdvertisement);
    }

    public AdvertisementEntity displayAdById(Long id){
        return advertisementRepository.findById(id).orElseThrow(() -> new ApplicationException(ApplicationError.ADVERTISEMENT_DOESNT_EXIST, id));
    }

    public AdvertisementResponseDto getById(Long id){
        return advertisementRepository.findById(id)
                .map(x-> converters.convert(x, AdvertisementResponseDto.class))
                .orElseThrow(()-> new ApplicationException(ApplicationError.ADVERTISEMENT_DOESNT_EXIST, id));
    }

    @Transactional
    public void addAdvertisementToFavorite(String username, Long id){

        AdvertisementEntity adById = advertisementRepository.findById(id).orElseThrow();
        UserEntity findByUsername = userRepository.findByUsername(username).orElseThrow();
        if(findByUsername.getFavorite()==null){
            findByUsername.setFavorite(new HashSet<>());
        }
        Set<AdvertisementEntity> favorite = findByUsername.getFavorite();
        favorite.add(adById);
    }

    @Transactional
    public void deleteAdvertisement(Long id) {
        AdvertisementEntity advertisementEntity = advertisementRepository.findById(id).orElseThrow(() -> new ApplicationException(ApplicationError.ADVERTISEMENT_DOESNT_EXIST, id));
        advertisementEntity.getUser().getAdvertisements().remove(advertisementEntity);
        advertisementRepository.delete(advertisementEntity);
    }

    @Transactional
    public AdvertisementResponseDto editAdvertisement(AdvertisementEntity request, Long id) {
        AdvertisementEntity adv = advertisementRepository.findById(id).orElseThrow();
        updateByRequestData(request, adv);
        AdvertisementEntity updatedAdv = advertisementRepository.save(adv);
        return converters.convert(updatedAdv, AdvertisementResponseDto.class);
    }

    @Transactional
    public void updateByRequestData(AdvertisementEntity request, AdvertisementEntity advertisement) {

        if (request.getTitle() != null) {
            advertisement.setTitle(request.getTitle());
        }
        if (Objects.nonNull(request.getContent())) {
            advertisement.setContent(request.getContent());
        }
        Optional.ofNullable(request.getPublishingDate()).ifPresent(advertisement::setPublishingDate);
        Optional.ofNullable(request.getCompanyName()).ifPresent(advertisement::setCompanyName);
        Optional.ofNullable(request.getSalary()).ifPresent(advertisement::setSalary);
        Optional.ofNullable(request.getLocation()).ifPresent(advertisement::setLocation);
        Optional.ofNullable(request.getExperience()).ifPresent(advertisement::setExperience);
        Optional.ofNullable(request.getContractType()).ifPresent(advertisement::setContractType);
        Optional.ofNullable(request.getTechnology()).ifPresent(advertisement::setTechnology);

    }
}
