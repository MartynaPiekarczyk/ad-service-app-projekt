package com.adserviceapp.service;

import com.adserviceapp.exception.ApplicationError;
import com.adserviceapp.exception.ApplicationException;
import com.adserviceapp.model.dao.LocationEntity;
import com.adserviceapp.repository.LocationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class LocationService {

    private final LocationRepository repository;

    public List<LocationEntity> listAllLocations() {
        return repository.findAll();
    }

    public LocationEntity getLocationById(Long id) {
        return repository.findById(id).orElseThrow(() -> new ApplicationException(ApplicationError.LOCATION_NOT_FOUND, id));
    }

    public List<LocationEntity> listLocationById(Long id) {
        return repository.findAllById(Collections.singleton(id));
    }
}
