package com.adserviceapp.service;

import com.adserviceapp.exception.ApplicationError;
import com.adserviceapp.exception.ApplicationException;
import com.adserviceapp.model.dao.RoleEntity;
import com.adserviceapp.repository.RoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class RoleService {
    private final RoleRepository repository;

    public List<RoleEntity> listAllRoles() {
        return repository.findAll();
    }

    public RoleEntity getRoleById(Long id) {
        return repository.findById(id).orElseThrow(() -> new ApplicationException(ApplicationError.ROLE_NOT_FOUND, id));
    }

    public List<RoleEntity> listRoleById(Long id) {
        return repository.findAllById(Collections.singleton(id));
    }
}
