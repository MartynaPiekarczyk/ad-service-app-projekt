package com.adserviceapp.service;

import com.adserviceapp.exception.ApplicationError;
import com.adserviceapp.exception.ApplicationException;
import com.adserviceapp.exception.BadArgsException;
import com.adserviceapp.model.dao.UserEntity;
import com.adserviceapp.model.dto.UserRequestDto;
import com.adserviceapp.model.dto.UserResponseDto;
import com.adserviceapp.model.dto.converters.Converters;
import com.adserviceapp.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository repository;
    //    private final PasswordEncoder encoder;
    private final Converters converters;

    public UserResponseDto addUser(UserRequestDto request) {
        boolean userExists = repository.existsByUsernameOrEmail(request.getUsername(), request.getEmail());

        if (userExists) {
            boolean usernameExists = repository.existsByUsername(request.getUsername());
            boolean emailExists = repository.existsByEmail(request.getEmail());

            BadArgsException exception = new BadArgsException();
            if (usernameExists) {
                exception.getErrors().put("usernameError", "Username already exists");
            }
            if (emailExists) {
                exception.getErrors().put("emailAddressError", "Email address already exists");
            }
            throw exception;
        }
        UserEntity newEntity = converters.convert(request, UserEntity.class);
        UserEntity savedEntity = repository.save(newEntity);
        return converters.convert(savedEntity, UserResponseDto.class);
    }

    public UserEntity findUser(String username) {
        return repository.findByUsername(username).orElseThrow(() -> new ApplicationException(ApplicationError.USER_DOES_NOT_EXIST, username));
    }

    public List<UserEntity> displayAllUsers() {
        return repository.findAll();
    }

    @Transactional
    public void deleteUser(Long id) {
        Optional.ofNullable(repository.existsById(id))
                .filter(x -> x)
                .orElseThrow(() -> new ApplicationException(ApplicationError.USER_DOES_NOT_EXIST, id));
        repository.deleteById(id);
    }
}
