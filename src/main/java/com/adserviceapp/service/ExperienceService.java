package com.adserviceapp.service;

import com.adserviceapp.exception.ApplicationError;
import com.adserviceapp.exception.ApplicationException;
import com.adserviceapp.model.dao.ExperienceEntity;
import com.adserviceapp.repository.ExperienceRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ExperienceService {

    private final ExperienceRepository repository;

    public List<ExperienceEntity> listAllExperiences() {
        return repository.findAll();
    }

    public ExperienceEntity getExperienceLevelById(Long id) {
        return repository.findById(id).orElseThrow(() -> new ApplicationException(ApplicationError.EXPERIENCE_NOT_FOUND, id));
    }


    public List<ExperienceEntity> listExperienceById(Long id) {
        return repository.findAllById(Collections.singleton(id));
    }
}
