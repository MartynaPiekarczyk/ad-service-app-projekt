package com.adserviceapp.repository;

import com.adserviceapp.model.dao.ExperienceEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExperienceRepository extends JpaRepository<ExperienceEntity, Long> {


}