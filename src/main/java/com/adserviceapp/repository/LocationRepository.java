package com.adserviceapp.repository;

import com.adserviceapp.model.dao.LocationEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<LocationEntity, Long> {
}
