package com.adserviceapp.repository;

import com.adserviceapp.model.dao.TechnologyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TechnologyRepository extends JpaRepository<TechnologyEntity, Long> {
}
