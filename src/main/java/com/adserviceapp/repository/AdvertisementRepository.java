package com.adserviceapp.repository;

import com.adserviceapp.model.dao.AdvertisementEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface AdvertisementRepository extends JpaRepository<AdvertisementEntity, Long> {
    Optional<AdvertisementEntity> findById(Long id);

    List<AdvertisementEntity> findAllByUserUsername(String username);
}
