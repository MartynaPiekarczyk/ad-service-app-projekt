package com.adserviceapp.repository;

import com.adserviceapp.model.dao.ContractTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContractTypeRepository extends JpaRepository<ContractTypeEntity, Long> {
}
