package com.adserviceapp.controller.front;


import com.adserviceapp.model.dao.*;
import com.adserviceapp.model.dto.AdvertisementRequestDto;
import com.adserviceapp.model.dto.converters.Converters;
import com.adserviceapp.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class AdvertisementController {

    private final AdvertisementService advertisementService;
    private final ContractTypeService contractTypeService;
    private final ExperienceService experienceService;
    private final LocationService locationService;
    private final TechnologyService technologyService;

    @GetMapping
    public String homePage() {
        return "redirect:/home";
    }

    @GetMapping("/home")
    public String homePage(Model model, @AuthenticationPrincipal User user) {
        List<AdvertisementEntity> advertisementList = advertisementService.displayAllAdvertisements();
        model.addAttribute("advertisementList", advertisementList);
        boolean isUserLogged = !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken);
        model.addAttribute("isUserLogged", isUserLogged);
        model.addAttribute("currentUserRole", getCurrentUserRoles(user));
        if (isUserLoggedIn(user)) {
            List<AdvertisementEntity> advertisementListByUserId = advertisementService.displayAddByUsername(user.getUsername());
            model.addAttribute("advertisementListByUserId", advertisementListByUserId);
        }
        if (isUserLoggedIn(user)) {
            Set<AdvertisementEntity> favAdvertisement = advertisementService.displayFavByUsername(user.getUsername());
            model.addAttribute("favAdvertisement", favAdvertisement);
        }
        return "index";
    }

    @GetMapping("/adv-add")
    public String addingAdvertisementView(Model model) {
        List<ContractTypeEntity> allContractTypes = contractTypeService.listAllContractTypes();
        List<ExperienceEntity> allExperienceLevels = experienceService.listAllExperiences();
        List<LocationEntity> allLocations = locationService.listAllLocations();
        List<TechnologyEntity> allTechnologies = technologyService.listAllTechnologies();
        model.addAttribute("allContractTypes", allContractTypes);
        model.addAttribute("allExperienceLevels", allExperienceLevels);
        model.addAttribute("allLocations", allLocations);
        model.addAttribute("allTechnologies", allTechnologies);
        model.addAttribute("advertisementRequestDto", new AdvertisementRequestDto());
        return "add-advertisement";
    }

    @PostMapping("/adv-add")
    public String createNewAdvertisement(@Valid @ModelAttribute("userRequestDto") AdvertisementRequestDto request,
                                         Errors errors, Model model, Principal principal) {
        if (errors.hasErrors()) {
            model.addAttribute("advertisementRequestDto", request);
            return "add-advertisement";
        } else {
            advertisementService.addAdvertisement(request, principal.getName());
            return "redirect:/home";
        }
    }

    @GetMapping("/advertisement/{id}")
    public String advertisementView(@PathVariable Long id, Model model) {
        AdvertisementEntity adById = advertisementService.displayAdById(id);
        List<ContractTypeEntity> ContractTypesById = adById.getContractType();
        ExperienceEntity ExperienceLevelsById = adById.getExperience();
        LocationEntity LocationsById = adById.getLocation();
        List<TechnologyEntity> TechnologiesById = adById.getTechnology();
        model.addAttribute("contractTypesById", ContractTypesById);
        model.addAttribute("experienceLevelsById", ExperienceLevelsById);
        model.addAttribute("locationsById", LocationsById);
        model.addAttribute("technologiesById", TechnologiesById);
        model.addAttribute("advertisement", adById);
        return "advertisement";
    }

    @GetMapping("/update-advertisement/{id}")
    public String updateAdvertisement(@PathVariable Long id, Model model) {
        List<ContractTypeEntity> allContractTypes = contractTypeService.listAllContractTypes();
        List<ExperienceEntity> allExperienceLevels = experienceService.listAllExperiences();
        List<LocationEntity> allLocations = locationService.listAllLocations();
        List<TechnologyEntity> allTechnologies = technologyService.listAllTechnologies();
        model.addAttribute("allContractTypes", allContractTypes);
        model.addAttribute("allExperienceLevels", allExperienceLevels);
        model.addAttribute("allLocations", allLocations);
        model.addAttribute("allTechnologies", allTechnologies);
        model.addAttribute("advertisementRequestDto", new AdvertisementRequestDto());
        AdvertisementEntity adById = advertisementService.displayAdById(id);
        List<ContractTypeEntity> ContractTypesById = adById.getContractType();
        ExperienceEntity ExperienceLevelsById = adById.getExperience();
        LocationEntity LocationsById = adById.getLocation();
        List<TechnologyEntity> TechnologiesById = adById.getTechnology();
        model.addAttribute("contractTypesById", ContractTypesById);
        model.addAttribute("experienceLevelsById", ExperienceLevelsById);
        model.addAttribute("locationsById", LocationsById);
        model.addAttribute("technologiesById", TechnologiesById);
        model.addAttribute("advertisement", adById);
        return "update-advertisement";
    }

    @PostMapping(path = "/update-advertisement/{id}")
    public String editAdvertisement(@Valid @ModelAttribute("userRequestDto") AdvertisementEntity request, @PathVariable Long id,
                                    Errors errors, Model model) {
        if (errors.hasErrors()) {
            model.addAttribute("advertisementRequestDto", request);
            return "update-advertisement";
        } else {
            advertisementService.editAdvertisement(request, id);
            return "redirect:/advertisement/{id}";
        }
    }

    @GetMapping("/add-fav/{id}")
    public String addAdvertisementToFavorite(@PathVariable(name = "id") Long id, @AuthenticationPrincipal User user) {
        advertisementService.addAdvertisementToFavorite(user.getUsername(), id);
        return "redirect:/home";
    }

    @GetMapping("/delete-fav/{id}")
    public String deleteFavAdvertisement(@PathVariable(name = "id") Long id, @AuthenticationPrincipal User user) {
        advertisementService.deleteFavByUsername(user.getUsername(), id);
        return "redirect:/home";
    }

    @GetMapping("/delete/{id}")
    public String deleteAdvertisement(@PathVariable(name = "id") Long id) {
        advertisementService.deleteAdvertisement(id);
        return "redirect:/home";
    }

    private boolean isUserLoggedIn(@AuthenticationPrincipal User user) {
        return Objects.nonNull(user) && Objects.nonNull(user.getUsername());
    }

    private String getCurrentUserRoles(User user) {
        return Optional.ofNullable(user)
                .map(usr -> usr.getAuthorities())
                .filter(auth -> auth.size() == 1)
                .map(auth -> auth.stream().findAny().orElse(null))
                .map(authority -> authority.getAuthority())
                .orElse("NONE");
    }
}

