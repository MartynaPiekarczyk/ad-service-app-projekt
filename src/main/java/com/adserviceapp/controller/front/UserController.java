package com.adserviceapp.controller.front;

import com.adserviceapp.exception.BadArgsException;
import com.adserviceapp.model.dao.RoleEntity;
import com.adserviceapp.model.dto.UserRequestDto;
import com.adserviceapp.service.RoleService;
import com.adserviceapp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequiredArgsConstructor
public class UserController {

    private final UserService service;
    private final RoleService roleService;

    @GetMapping("/user/login")
    public String loginView(Model model) {
        model.addAttribute("userRequestDto", new UserRequestDto());
        return "login";
    }

    @GetMapping("/user/register")
    public String registrationView(Model model) {
        List<RoleEntity> allRoles = roleService.listAllRoles().stream()
                .filter(r -> !r.getRolename().equalsIgnoreCase("admin"))
                .collect(Collectors.toList());
        model.addAttribute("allRoles", allRoles);
        model.addAttribute("userRequestDto", new UserRequestDto());
        return "registration";
    }

    @PostMapping("/user/register")
    public String createNewUser(@Valid @ModelAttribute("userRequestDto") UserRequestDto request, Errors errors, Model model) {
        if (errors.hasErrors()) {
            model.addAttribute("userRequestDto", request);
            return "registration";
        } else {
            try {
                service.addUser(request);
            } catch (BadArgsException ex) {
                ex.getErrors().entrySet().forEach(entry -> model.addAttribute(entry.getKey(), entry.getValue()));
                ex.printStackTrace();
                return "registration";
            }
            return "redirect:/user/registration-success";
        }
    }

    @GetMapping("/user/registration-success")
    public String registrationSuccessView(Model model) {
        model.addAttribute("userRequestDto", new UserRequestDto());
        return "registration-success";
    }

}
