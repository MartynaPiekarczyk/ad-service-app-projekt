package com.adserviceapp.controller.rest;

import com.adserviceapp.model.dao.UserEntity;
import com.adserviceapp.model.dto.UserRequestDto;
import com.adserviceapp.model.dto.UserResponseDto;
import com.adserviceapp.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping(path = "/rest")
public class RestUserController {

    private final UserService userService;

    @PostMapping(path = "/add-user")
    public UserResponseDto addNewUser(@RequestBody UserRequestDto request) {
        return userService.addUser(request);
    }

    @GetMapping(path = "/all-users")
    public List<UserEntity> getAllUsers() {
        return userService.displayAllUsers();
    }
}
