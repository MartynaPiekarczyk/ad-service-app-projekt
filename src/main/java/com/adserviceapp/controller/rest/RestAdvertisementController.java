package com.adserviceapp.controller.rest;

import com.adserviceapp.model.dao.AdvertisementEntity;
import com.adserviceapp.model.dto.AdvertisementRequestDto;
import com.adserviceapp.model.dto.AdvertisementResponseDto;
import com.adserviceapp.service.AdvertisementService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping(path = "/rest")
public class RestAdvertisementController {

    private final AdvertisementService advertisementService;

    @PostMapping(path = "/add-advertisement")
    public AdvertisementResponseDto addNewAdvertisement(@RequestBody AdvertisementRequestDto request, Principal principal) {
        return advertisementService.addAdvertisement(request, principal.getName());
    }

    @GetMapping(path = "/all-advertisements")
    public List<AdvertisementEntity> getAllAdvertisements() {
        return advertisementService.displayAllAdvertisements();
    }

    @DeleteMapping(path = "/delete-adverisement/{id}")
    public void deleteAdvertisementById(@PathVariable Long id) {
        advertisementService.deleteAdvertisement(id);
    }

    @PostMapping(path = "/update-advertisement/{id}")
    public AdvertisementResponseDto updateAdvertisement(@RequestBody AdvertisementEntity entity, @PathVariable Long id) {
        return advertisementService.editAdvertisement(entity, id);
    }
}
