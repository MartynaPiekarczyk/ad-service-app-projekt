FROM openjdk:11.0.14.1-jdk-slim-bullseye
ADD target/ad-service-app-0.0.1-SNAPSHOT.jar .
EXPOSE $PORT
CMD java -jar ad-service-app-0.0.1-SNAPSHOT.jar