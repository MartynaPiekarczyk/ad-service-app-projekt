export class Registration {

    userNumber = Math.floor(Math.random() * 1000)

    newUser() {
        cy.get('[id="role-list"]').select(2)
        cy.get('[id="username"]').click().type('newuser' + this.userNumber)
        cy.get('[id="password"]').click().type('newuser' + this.userNumber)
        cy.get('[id="email"]').click().type('newuser' + this.userNumber + '@gmail.com')
        cy.contains('button', 'Zarejestruj!').click()
    }

    newEmployer() {
        cy.get('[id="role-list"]').select(1)
        cy.get('[id="username"]').click().type('newemployer' + this.userNumber)
        cy.get('[id="password"]').click().type('newemployer' + this.userNumber)
        cy.get('[id="email"]').click().type('newemployer' + this.userNumber + '@gmail.com')
        cy.contains('button', 'Zarejestruj!').click()
    }

    properRegistrationAssertions() {
        cy.wait(500)
        cy.get('[class="success-message"]').should('contain.text', 'Twoje konto zostało pomyślnie utworzone!')
        cy.location('pathname').should('contain', 'registration-success')
        cy.wait(7000)
        cy.location('pathname').should('contain', 'home')
    }

    duplicatedUser() {
        cy.get('[id="role-list"]').select(1)
        cy.get('[id="username"]').click().type(Cypress.env("user").employer.login)
        cy.get('[id="password"]').click().type(Cypress.env("user").employer.password)
        cy.get('[id="email"]').click().type(Cypress.env("duplicatedUserEmail"))
        cy.contains('button', 'Zarejestruj!').click()
    }

    duplicatedRegistrationAssertions() {
        cy.contains('[class="error-message"]', 'Username already exists' || 'Email address already exists')
        cy.location('pathname').should('contain', '/register')
    }

    withWrongCredentials() {
        cy.get('[id="role-list"]').select(2)
        cy.get('[id="username"]').click().type(Cypress.env("user").wrongCredentials.login)
        cy.get('[id="password"]').click().type(Cypress.env("user").wrongCredentials.password)
        cy.get('[id="email"]').click().type('user@gmail.com')
        cy.contains('button', 'Zarejestruj!').click()
    }

    wrongCredentialsAssertions() {
        cy.contains('[class="error-message"]', 'Nazwa użytkownika musi mieć minimalnie 6, a maksymalnie 30 znaków.' && 'Hasło musi mieć minimalnie 8, a maksymalnie 30 znaków.')
        cy.location('pathname').should('contain', '/register')
    }
}

export const register = new Registration()