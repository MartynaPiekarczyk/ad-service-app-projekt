import {login} from "./login";

export class Advertisement {

    newAdvertisement() {
        login.userAccount(Cypress.env("user").employer)
        cy.get('[id="add-adv-link"]').click()
        cy.get('[id="title"]').click().type(Cypress.env("advertisement").title)
        cy.get('[id="input-value-content"]').click().type(Cypress.env("advertisement").content)
        cy.get('[id="companyName"]').click().type(Cypress.env("advertisement").companyName)
        cy.get('[id="salary"]').click().type(Cypress.env("advertisement").salary)
        cy.get('[id="localization-list"]').select(17)
        cy.get('[id="experience-list"]').select(4)
        cy.get('[id="contractType-list"]').select(2)
        cy.get('[id="technology-list"]').select(6)
        cy.contains('button', 'Dodaj').click()
    }

    advertisementAssertions() {
        cy.get('td').should('contain.text', Cypress.env("advertisement").title)
        cy.location('pathname').should('contain', '/home')
    }
}

export const add = new Advertisement()