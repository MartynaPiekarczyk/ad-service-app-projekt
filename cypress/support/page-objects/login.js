export class Login {
    login(user) {
        cy.get('[id="username"]').click().type(user.login)
        cy.get('[id="password"]').click().type(user.password)
        cy.contains('button', 'Zaloguj!').click()
    }

    userAccount(user) {
        this.login(user)
        cy.get('a').should('contain.text', 'Wyloguj')
    }

    userAssertions() {
        cy.get('h2').should('contain.text', 'Moje ulubione ogłoszsenia')
    }

    employerAssertions() {
        cy.get('h2').should('contain.text', 'Lista moich ogłoszeń')
    }

    adminAssertions() {
        cy.get('[id="delete-adv"]').should('contain.text', 'Usuń')
    }

    logout() {
        cy.get('[id="logout-link"]').click()
        cy.get('a').should('contain.text', 'Logowanie')
    }
}

export const login = new Login()
export const logout = new Login()