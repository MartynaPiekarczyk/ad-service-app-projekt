import {register} from "../support/page-objects/registration";

describe('User registration tests', () => {
    beforeEach('Open Job Advertisements Service', () => {
        cy.visit('/')
        cy.get('[id="registration-form"]').click()
    })

    it("Register new user", () => {
        register.newUser()
        register.properRegistrationAssertions()
    })

    it('Register new employer', () => {
        register.newEmployer()
        register.properRegistrationAssertions()
    })

    it('Register duplicated user', () => {
        register.duplicatedUser()
        register.duplicatedRegistrationAssertions()
    })

    it('Register user with wrong username and password', () => {
        register.withWrongCredentials()
        register.wrongCredentialsAssertions()

    })
})