import {add} from "../support/page-objects/advertisement";

describe('Adding new advertisement test', () => {

    beforeEach('Open Job Advertisements Service', () => {
        cy.visit('/')
    })

    it('Add new advertisement', () => {
        add.newAdvertisement()
        add.advertisementAssertions()
    })
})