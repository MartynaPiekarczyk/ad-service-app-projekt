import {login, logout} from "../support/page-objects/login";

describe('Logging in and logging out tests', () => {

    beforeEach('Open Job Advertisements Service', () => {
        cy.visit('/')
    })

    it(`Log in test for ${Cypress.env("user").user.login} user`, () => {
        login.userAccount(Cypress.env("user").user)
        login.userAssertions()
    })

    it(`Log in test for ${Cypress.env("user").employer.login} user`, () => {
        login.userAccount(Cypress.env("user").employer)
        login.employerAssertions()
    })

    it(`Log in test for ${Cypress.env("user").admin.login} user`, () => {
        login.userAccount(Cypress.env("user").admin)
        login.adminAssertions()
    })

    it.only(`Log out test`, () => {
        login.userAccount(Cypress.env("user").user)
        logout.logout()
    })
})
